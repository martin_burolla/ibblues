# Nib Notes [![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=56f29e71663f63010002efef&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/56f29e71663f63010002efef/build/latest)

A helper project that illustrates how to use ```Nib``` files with your iOS projects.

<p align="center">
<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/IBBlues/home.png  width=200 length=400>
<p>

## Nibs as Reusable Widgets

### The Widget

The blue squares shown in the picture above is a ```Nib``` file instantiated twice.

Create the ```Nib``` file in Interface Builder and create a class which subclasses ```UIView```.  Link the ```Nib``` file to your class as shown below:

<p align="center">
<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/IBBlues/widgetIB2.png> 
<p>

Connect the UI components in the ```Nib``` to the implementation.

<p align="center">
<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/IBBlues/widgetIB.png> 
<p>

```

#import "SleepsView.h"

@interface SleepsView()
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (assign) int count;
@end

@implementation SleepsView

#pragma mark - Lifecycle

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    self.count = 0;
    return self;
}

#pragma mark - User Interaction

- (IBAction)onIncrement:(id)sender {
    ++self.count;
    self.countLabel.text = [@(self.count) stringValue];
}

- (IBAction)onDecrement:(id)sender {
    --self.count;
    self.countLabel.text = [@(self.count) stringValue];
}

@end
```
### Consuming the Widget

Create two ```UIView```s on the parent view and connect them to the implementation file in the View Controller.

<p align="center">
<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/IBBlues/UIIB.png> 
<p>

```
@interface ViewController ()
@property (weak, nonatomic) IBOutlet SleepsView *minSleeps;
@property (weak, nonatomic) IBOutlet SleepsView *maxSleeps;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Load a XIB file as a UIView.
    SleepsView *minView = [[[NSBundle mainBundle] loadNibNamed:@"Sleeps" owner:self options:nil] objectAtIndex:0];
    minView.headerLabel.text = @"Min Sleeps";
    [self.minSleeps addSubview: minView];
    
    SleepsView *maxView = [[[NSBundle mainBundle] loadNibNamed:@"Sleeps" owner:self options:nil] objectAtIndex:0];
    maxView.headerLabel.text = @"Max Sleeps";
    [self.maxSleeps addSubview: maxView];
}

@end
```

Add constraints to these views.

<p align="center">
<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/IBBlues/constraints.png> 
<p>


## Multiple UI Elements in a Nib

The bottom half of the screen in this iOS app illustrates how to use multiple UI components in a single Nib file.

Multiple UI elements can be stored in a single ```Nib``` file.  The ```File's Owner``` in the ```Nib``` file has to be set to your ```ViewController``` class.  In this example a ```UIView``` contains a ```UITableView```.  The view outlet in the ```Nib``` file must be set to this ```UIView```.

<p align="center">
<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/IBBlues/AnimalPicker.png> 
<p>

Make sure to call ```self addChildViewController``` in the parent ```ViewController``` or else the ```UITableViewDelegate``` calls will not get fired.

The FilesOwner contains ```IBOutlets``` linked to the ```Nib``` file:

```
@interface MyPickerViewController () <UITableViewDataSource>
    @property (weak, nonatomic) IBOutlet UITableView *tableView;
    @property (strong, nonatomic) IBOutlet CatsTableViewCell *catsTableViewCell;
    @property (strong, nonatomic) IBOutlet DogsTableViewCell *dogsTableViewCell;
    @property (strong, nonatomic) NSArray *rows;
@end
```

The Cat and Dog cells are ```UITableView``` cells that connect to the Nib file using and ```IBAction```.

```
#import "CatsTableViewCell.h"

@implementation CatsTableViewCell

- (IBAction)onCatIncrease:(id)sender {
    NSLog(@"Cat increase.");
}

@end
```

The File's Owner for the ```Nib``` file looks like this:

<p align="center">
<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/IBBlues/IB%20Files%20Owner.png> 
<p>


<img src=https://raw.githubusercontent.com/mburolla/ReadMePix/master/HelperRepos/hr.png width=1024 height=120>


