//
//  MyPickerViewController.h
//  IBBlues
//
//  Created by Martin Burolla on 3/22/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPickerViewController : UIViewController

-(instancetype)init;

@end
