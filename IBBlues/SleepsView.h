//
//  MyView.h
//  IBBlues
//
//  Created by Martin Burolla on 2/29/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SleepsView : UIView
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@end
