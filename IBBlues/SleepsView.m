//
//  MyView.m
//  IBBlues
//
//  Created by Martin Burolla on 2/29/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

#import "SleepsView.h"

@interface SleepsView()
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (assign) int count;
@end

@implementation SleepsView

// MARK: - Lifecycle

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    self.count = 0;
    return self;
}

#pragma mark - User Interaction

- (IBAction)onIncrement:(id)sender {
    ++self.count;
    self.countLabel.text = [@(self.count) stringValue];
}

- (IBAction)onDecrement:(id)sender {
    --self.count;
    self.countLabel.text = [@(self.count) stringValue];
}

@end
