//
//  main.m
//  IBBlues
//
//  Created by Martin Burolla on 2/29/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
