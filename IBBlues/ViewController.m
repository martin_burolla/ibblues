//
//  ViewController.m
//  IBBlues
//
//  Created by Martin Burolla on 2/29/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

#import "ViewController.h"
#import "SleepsView.h"
#import "MyPickerViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet SleepsView *minSleeps;
@property (weak, nonatomic) IBOutlet SleepsView *maxSleeps;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpViews];
}

- (void) setUpViews {
    
    // Load a XIB file as a UIView (left side)
    SleepsView *minView = [[[NSBundle mainBundle] loadNibNamed:@"Sleeps" owner:self options:nil] objectAtIndex:0];
    minView.headerLabel.text = @"Min Sleeps";
    [self.minSleeps addSubview: minView];
    
    // Load a XIB file as a UIView (right side)
    SleepsView *maxView = [[[NSBundle mainBundle] loadNibNamed:@"Sleeps" owner:self options:nil] objectAtIndex:0];
    maxView.headerLabel.text = @"Max Sleeps";
    [self.maxSleeps addSubview: maxView];
    
    // Load the bottom child container view with the Animal Picker.
    MyPickerViewController *pickerViewController = [[MyPickerViewController alloc] init];
    [self addChildViewController:pickerViewController];
    [self.containerView addSubview:pickerViewController.view];
}

@end
