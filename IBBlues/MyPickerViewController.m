//
//  MyPickerViewController.m
//  IBBlues
//
//  Created by Martin Burolla on 3/22/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

#import "MyPickerViewController.h"
#import "CatsTableViewCell.h"
#import "DogsTableViewCell.h"

@interface MyPickerViewController () <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet CatsTableViewCell *catsTableViewCell;
@property (strong, nonatomic) IBOutlet DogsTableViewCell *dogsTableViewCell;
@property (strong, nonatomic) NSArray *rows;
@end

@implementation MyPickerViewController 

#pragma - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.rows = @[self.catsTableViewCell, self.dogsTableViewCell];
    
    //self.tableView.dataSource = self; // Not required, because this is set in Interface Builder.
}

-(instancetype)init {
    self = [super initWithNibName: NSStringFromClass([MyPickerViewController class]) bundle:nil];
    if (self != nil) {
    }
    return self;
}

#pragma - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.rows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.rows[indexPath.row];
}

@end
