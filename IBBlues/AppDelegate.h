//
//  AppDelegate.h
//  IBBlues
//
//  Created by Martin Burolla on 2/29/16.
//  Copyright © 2016 Martin Burolla. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

